# NfcUpdater

Interfaz de comunicacion entre el API de [PTPS](https://gitlab.com/fmanuelng/publicTransportPaymentSystem) y el controlador de arduino [ChelitosArduinoController](https://gitlab.com/fmanuelng/arduinoController)

<!-- TOC -->

- [NfcUpdater](#nfcupdater)
    - [Funcionalidades](#funcionalidades)
    - [Instalacion](#instalacion)
    - [Uso](#uso)
    - [Herramientas Utilizadas](#herramientas-utilizadas)

<!-- /TOC -->

## Funcionalidades

1. Capacidad de comunicarse con el dispositivo Arduino mediante el puerto serial del computador.
2. Posibilidad de hacer el registro de una tarjeta NFC desde la plataforma misma.
3. Capasidad de leer la tarjeta NFC para saber su monto actual y buscar los demas detalles dentro del API de [PTPS](https://gitlab.com/fmanuelng/publicTransportPaymentSystem) de forma automatica.

## Instalacion
Para ser instalado primero debemos de cumplir con los siguientes requerimientos:

1. Tener instalado el compilador del lenguaje de programacion Golang, puede obtenerlo [aqui](https://golang.org/).
2. Tener instalado Git en su computador, para distribuciones Windows y Mac puede obtenerlo [aqui](https://git-scm.com/). Para distribuciones linux debera de buscar el como instalarlo para su sabor de distro.

Una vez haya cumplido con los requisitos pautados debera hacer lo siguiente:

1. Debe de abrir una consola o cmd y teclear el siguiente comando:
            `go get gitlab.com/fmanuelng/nfcUpdater/...`
2. Luego debera de aceder a la ubicacion de su GOPATH (mas informacion [aqui](https://github.com/golang/go/wiki/SettingGOPATH), aunque ya por defecto golang al momento de ejecutar el comando go get genero una carpeta denominada *go* en su carpeta de usuario, en windows esta se ubica en /users/{miUsuaio}/go, y en sistemas mac y linux esta en ~/go) y seguir la ruta gitlab.com/fmanuelng/nfcUpdater/
3. Una vez dentro de esta carpeta abra una consola dentro de esta y ejecute el siguiente comando: `go get ./` para asi obtener todas las dependencias que contiene el proyecto en si.
4. Una vez terminada toda la operacion de descargas de dependencias ejecute este comando para generar un ejecutable para su sistema operativo: `go build` y luego `go run`

## Uso
Una vez generado el ejecutable puede abrir una consola en la ruta actual del ejecutable y ver los comandos admitidos por el sistema mediante el comando `./nfcUpdater -h` y el cual lanzara una lista con los siguientes parametros soportados:

* `-comPort={port}`: Ubicacion del puerto donde el arduino esta conectado actualmente. Por defecto se tiene que el puerto usado para maquinas linux es `/dev/ttyUSB0`. para computadores windows el puerto debe de ser `COM{number}` donde *number* es el numero de puerto usado en windows por el arduino como lo podria ser el `COM5`. para otra ubicacion de puerto serial solo debe de ingresar el comando mencionado con el puerto a utilizar como parametro.
* `-baud={number}`: Frecuencia en baudios con la que se leera el puerto serial de nuestro arduino. Por defecto es el `9600`. De ser el arduino programado con una cantidad de baudios distinta favor de ingresar este valor como parametro.
* `-apiUrl={url}`: Direccion web donde se encuentra en ejecucion el PTPS, por defecto la ruta es `http://localhost:8080/`. Si la ruta por defecto no es la correcta en su sistema favor de introducirla como parametro.

Una ves ingresado al sistema le aparecera la siguiente pantalla:  

`   Operacion a realizar:
		
		1- Leer monto de la tarjeta NFC
		2- Escribir nuevo monto en la tarjeta NCF
		
		Opcion:
`
En la cual debera de ingresar el numero de opcion a escoger

En caso de elegir la opcion 1 debera de acercar la tarjeta NFC que desee leer y esperar que el arduino retorne la informacion al sistema para luego verla en pantalla. La informacion recibida puede llegar a verse como el siguiene ejemplo:  

`Data recibida de la tarjeta NFC leida: 
				
					ID: 40421654-44e9-11e8-842f-0ed5f89f718b
					Monto actual: 232.43`



## Herramientas Utilizadas

[tarm/serial]("github.com/tarm/serial"): Lector y escritor de puertos seriales