package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"

	"github.com/tarm/serial"
)

//RequestData estructura utilizada para el envio de imformacion hacia el arduino en cuestion
type RequestData struct {
	//CMD comando enviado al arduino para su inicializacion de rutina
	CMD rune `json:"cmd"`
	//Amount monto a ser almacenado en la tarjeta nfc
	Amount float32 `json:"amount"`
}

//ResponseData estructura utilizada para el manejo de la data contenida en una tarjeta nfc
type ResponseData struct {
	//NfcID id unico de la tarjeta nfc en cuestion
	NfcID string `json:"id"`
	//Amount monto contenido en la tarjeta NFC en cuestion
	Amount float32 `json:"amount"`
}

//PostChange envia el ResponseData al servidor Chelitos para su almacenamiento o modificacion de los datos
func (r *ResponseData) PostChange(urlPath string) (err error) {
	b, er := json.Marshal(r)
	if er != nil {
		err = er
		return
	}
	fullUrl := fmt.Sprintf("%s/api/arduino/setAmount", urlPath)
	req, er := http.NewRequest("POST", fullUrl, bytes.NewBuffer(b))
	if er != nil {
		err = er
		return
	}
	req.Header.Set("X-Custom-Header", "")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, er := client.Do(req)
	if er != nil {
		err = er
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf(`
		Data enviada. Respuesta del servidor:

		Estatus de respuesta: %s
		Cuerpo de la respuesta: %s
		`, resp.Status, string(body))
	return
}

var (
	conn    *serial.Port
	comPort = flag.String("comPort", "/dev/ttyUSB0", `Puerto de comunicacion al el cual se hara conexion
		
		Para sistemas linux y unix: Expesifique la ruta o path del dispositivo. Ejemplo: "/dev/ttyUSB0"
		
		Para sistemas windows: Expecifique el puerto de comunicaciones al cual se hara conexion. Ejemplo: "COM1"`)

	baud   = flag.Int("baud", 9600, `Frecuencia en baudios para la comunicacion con el arduino. Por defecto su frecuencia es de 9600`)
	apiUrl = flag.String("apiUrl", "http://localhost:8080", `URL del API de Chelitos a conectar. Por defecto es: "http://localhost:8080/"`)
)

func main() {
	flag.Parse()
	conn, err := serial.OpenPort(&serial.Config{Name: *comPort, Baud: *baud})
	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()
opcion:
	clearCMD()
	fmt.Println(`Operacion a realizar:
		
		1- Leer monto de la tarjeta NFC
		2- Escribir nuevo monto en la tarjeta NCF
		
		Opcion: `)

	var opc string

	fmt.Scanf("%s\n", &opc)

	if v, err := strconv.Atoi(opc); err != nil {
		log.Fatal(err)
	} else {
		switch v {
		case 1:
			request := &RequestData{
				CMD: 'r',
			}
			resp, err := request.ReadAmount()
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf(`Data recibida de la tarjeta NFC leida: 
				
					ID: %s
					Monto actual: %s`, resp.NfcID, strconv.FormatFloat(float64(resp.Amount), 'f', 3, 32))
		case 2:
			fmt.Println("Ingrese el monto a recargar: ")
			var amountStr string
			fmt.Scanf("%s\n", &amountStr)
			if v, err := strconv.ParseFloat(amountStr, 64); err != nil {
				goto opcion
			} else {
				request := &RequestData{
					CMD:    'w',
					Amount: float32(v),
				}
				err := request.WriteAmount(*apiUrl)
				if err != nil {
					log.Fatal(err)
				}
			}
		default:

		}
	}

}

//WriteAmount escribe el monto recibido como parametro en la tarjeta NFC y luego envia esta data al servidor web
func (r *RequestData) WriteAmount(urlPath string) (err error) {
	b, er := json.Marshal(r)
	if er != nil {
		err = er
		return
	}
	if _, er := conn.Write(b); er != nil {
		err = er
		return
	}
	var resp ResponseData
	resp.Amount = r.Amount
	er = resp.PostChange(urlPath)
	if er != nil {
		return
	}
	return
}

//ReadAmount lee la data contenida en la tarjeta NFC en cuestion
func (r *RequestData) ReadAmount() (resp *ResponseData, err error) {
	b := make([]byte, 4966)
	if _, er := conn.Write([]byte(`{"cmd":"r"}`)); er != nil {
		err = er
		return
	}
	if _, er := conn.Read(b); er != nil {
		err = er
		return
	}
	var response ResponseData
	if er := json.Unmarshal(b, &response); er != nil {
		err = er
		return
	}
	resp = &response
	return
}

func clearCMD() {
	switch runtime.GOOS {
	case "linux":
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	case "windows":
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}
